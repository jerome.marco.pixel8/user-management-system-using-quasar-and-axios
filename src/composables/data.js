import { ref } from "vue";
import axios from "axios";
import { rows } from "../pages/ListOfUser.vue";
import { form, userForm } from "../pages/AddUser.vue";

const Rows = ref([]);
const UserList = ref([]);
const UserInfo = ref([]);
const btnLoadingState = ref(false);

const submit = async () => {
  btnLoadingState.value = true;
  const response = await axios.post(
    "https://jsonplaceholder.typicode.com/users",
    form.value
  );
  if (response.status === 201) {
    const newUser = Rows.value.length + 1;
    const row = response.data;
    Rows.value.push({
      id: newUser,
      name: row.name,
      username: row.username,
      email: row.email,
      address: {
        ...row.address,
        geo: { ...row.address.geo },
      },
      phone: row.phone,
      website: row.website,
      company: { ...row.company },
    });
    form.value = {
      name: "",
      username: "",
      email: "",
      address: {
        street: "",
        suite: "",
        city: "",
        zipcode: "",
        geo: {
          lat: "",
          lng: "",
        },
      },
      phone: "",
      website: "",
      company: {
        name: "",
        catchPhrase: "",
        bs: "",
      },
    };
    userForm.value.reset();
  }
  btnLoadingState.value = false;
};

const getUsers = async () => {
  const response = await axios.get(
    "https://jsonplaceholder.typicode.com/users"
  );
  Rows.value = response.data;
  rows.value = Rows.value;
};

getUsers();

export { UserList, UserInfo, submit, getUsers, btnLoadingState };
